#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
struct move{
  char move;
};
struct grid{
  std::vector<std::vector<char>> grid;
  std::vector<std::vector<int>> t_visited;
  int x;
  int y;
  int old_x;
  int old_y;
  int old_t_x;
  int old_t_y;
  int t_x;
  int t_y;
};
// grid will just update the head to the x and y pos
grid update(grid g){
  //extend if necessary
  grid temp = g;
  if(g.y<g.old_y){
    std::vector<char> t;
    for(int i = 0;i!=temp.grid[0].size();i++)
      t.push_back('.');
    g.grid.push_back(t);
  }
  if(g.y>=g.grid.size()){
    std::vector<char> t;
    for(int i = 0;i!=temp.grid[0].size();i++)
      t.push_back('.');
    g.grid.push_back(t);
  }
  if(g.x<g.old_x){
    for(int i = 0;i!=temp.grid.size();i++){
      g.grid[i].push_back('.');
      }
  }
  if(g.x>=g.grid[0].size()){
    for(int i = 0;i!=temp.grid.size();i++){
      g.grid[i].insert(g.grid[i].begin(),1,'.');
      }
  }
  g.grid[g.grid.size()-g.old_y-1][g.old_x]='.';
  //handle tail movement first
  if(g.x>g.old_x){
    //moving right
    
  }
  //move head accordingly
  //g.grid[g.grid.size()-g.old_y-1][g.old_x]='.';
  g.grid[g.grid.size()-1-g.y][g.x]='H';
  g.old_x=g.x;
  g.old_y=g.y;
  return g;
}
void list(grid g){
  for(std::vector<char> c : g.grid){
    for(char cc : c)
      std::cout<<cc;
    std::cout<<std::endl;
  }
}
int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  grid grid = {{{'H'}},{{0,0}},0,0,0,0,0,0,0,0}; // B means both are in the same spot
  
  std::string line;
  std::vector<char> moves;
  while(getline(std::cin, line)){
    if(line==";")
      break;
    for(int i = 0;i!=line[2]-'0';i++)
      moves.push_back(line[0]);
    
    
  }
  for(int i = 0; i!=moves.size();i++){
    if(moves[i]=='U')
      grid.y+=1;
    if(moves[i]=='D')
      grid.y-=1;
    if(moves[i]=='R')
      grid.x+=1;
    if(moves[i]=='L')
      grid.x-=1;
    std::cout<<moves[i]<<" "<<grid.x<<grid.y<<std::endl;
    grid = update(grid);
    list(grid);
    std::cout<<std::endl<<std::endl;
  }
  std::cout<<std::endl;
  //std::cout<<grid.grid.size();
  return 0;
}