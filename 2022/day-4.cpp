#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>

int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  std::vector<int> inp;
  std::string line;
  std::vector<std::vector<int>> pair;
  int output = 0;
  int p2_output = 0;
  while(getline(std::cin, line)){
    if(line==";")
      break;
    std::vector<int> contains = {};
    std::vector<int> v1 = {};
    std::vector<int> v2 = {};
    //int s1 = line[0]-'0';
    //int e1 = line[2]-'0';
    //int s2 = line[4]-'0';
    //int e2 = line[6]-'0';
    std::string s1 = "";
    for(int i = 0; i!=line.size();i++){
    if(line[i]=='-')
        break;
    s1+=line[i];
    }
    int ps1 = stoi(s1);
    std::string e1 = "";
    for(int i = s1.size()+1; i!=line.size();i++){
    if(line[i]==',')
        break;
    e1+=line[i];
    }
    int pe1 = stoi(e1);
    std::string s2 = "";
    for(int i = s1.size()+e1.size()+2; i!=line.size();i++){
    if(line[i]=='-')
        break;
    s2+=line[i];
    }
    int ps2 = stoi(s2);
    std::string e2 = "";
    for(int i = s2.size()+s1.size()+e1.size()+3; i<=line.size();i++){
    e2+=line[i];
    }
    int pe2 = stoi(e2);
    
    for(int i = ps1; i!=pe1+1;i++)
        v1.push_back(i);
    for(int z = ps2; z!=pe2+1;z++)
        v2.push_back(z);
    int over = 0;
    for(int i = ps1; i!=pe1+1;i++){
        for(int z = ps2; z!=pe2+1;z++){
            if(i==z){
                contains.push_back(i);
                over=1;
            }
            
        }
        
    }
    if(over!=0)
        p2_output++;
    if(contains==v1||contains==v2)
        output++;
    
    
  }
  std::cout<<"part 1 :"<<output<<std::endl;
  std::cout<<"part 2 :"<<p2_output;
  return 0;
}