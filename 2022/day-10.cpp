#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  std::vector<int> inp;
  std::string line;
  std::vector<int> instructions;
  int reg = 1;
  int cyc = 0;
  while(getline(std::cin, line)){
    if(line==";")
      break;
    if(line.find("noop") != std::string::npos){
        instructions.push_back(0);
    } else {
        line.erase(0,5);
        instructions.push_back(0);
        instructions.push_back(stoi(line));
    }
  }
  int signal = 0;
  int crt_r = 0;
  int reg_2 = 0;
  std::string crt = "";
  for(int i : instructions){
    //std::cout<<i<<std::endl;
    cyc++;
    if(cyc==20||cyc==60||cyc==100||cyc==140||cyc==180||cyc==220){
        signal+=reg*cyc;
        //std::cout<<signal<<std::endl;
    }
    reg+=i;
    if(cyc%40==0)
      crt+="\n";
    else{
      std::cout<<reg<<" "<<cyc%40<<std::endl;
      if(reg==cyc%40||reg+1==cyc%40||reg-1==cyc%40){
        crt+='#';
      } else {
        crt+='.';
      }
    }
    
    

  }
  std::cout<<"part 1 :"<<signal<<std::endl<<crt;
  return 0;
}