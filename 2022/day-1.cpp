#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  std::vector<int> inp;
  std::string line;
  int index = 0;
  inp.push_back(0);
  while(getline(std::cin, line)){
    if(line==";")
      break;
    if(line==""){
      index++;
      inp.push_back(0);
    }
    else
      inp[index]+=std::stoi(line);
  }
  std::sort(inp.begin(),inp.end(),std::greater<int>());
  std::cout<<"part 1 :"<<inp[0]<<std::endl; 
  std::cout<<"part 2 :"<<inp[0]+inp[1]+inp[2];
  return 0;
}