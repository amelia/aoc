#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  std::vector<int> inp;
  std::string line;
  std::vector<std::string> trees;
  while(getline(std::cin, line)){
    if(line==";")
      break;
    trees.push_back(line);
  }
  int total = 0;
  int p2_score = 0;
  for(int i = 0; i!=trees.size()-1;i++){
    if(i!=0){
    for(int z = 0; z!= trees[i].size()-1;z++){
        if(z!=0){
            int thi = trees[i][z]-'0';
            /*
            int top = trees[i+1][z]-'0';
            int bottom = trees[i-1][z]-'0';
            int left = trees[i][z-1]-'0';
            int right = trees[i][z+1]-'0';*/
            int right = 0;
            for(int y = z+1; y!= trees[i].size();y++){
                if(trees[i][y]-'0'>right){
                    right = trees[i][y]-'0';
                }
            }
            //part 2 
            int right_dis = 0;
            for(int y = z+1; y!= trees[i].size();y++){
                right_dis++;
                if(trees[i][y]-'0'>=trees[i][z]-'0'){
                    break;
                }
                
            }
            //
            int left = 0;
            for(int y = 0; y!=z;y++){
                if(trees[i][y]-'0'>left){
                    left = trees[i][y]-'0';
                }
            }
            //part 2 
            int left_dis = 0;
            for(int y = z-1; y>= 0;y--){
                left_dis++;
                if(trees[i][y]-'0'>=trees[i][z]-'0'){
                    break;
                }
                
            }
            //
            int bottom = 0;
            for(int y = i+1; y!=trees.size();y++){
                if(trees[y][z]-'0'>bottom){
                    bottom = trees[y][z]-'0';
                }
            }
            //part 2 
            int bottom_dis = 0;
            for(int y = i+1; y!=trees.size();y++){
                bottom_dis++;
                if(trees[y][z]-'0'>=trees[i][z]-'0'){
                    break;
                }
                
            }
            //
            int top = 0;
            for(int y = 0; y!=i;y++){
                if(trees[y][z]-'0'>top){
                    top = trees[y][z]-'0';
                }
            }
            //part 2 
            int top_dis = 0;
            for(int y = i-1; y>=0;y--){
                top_dis++;
                if(trees[y][z]-'0'>=trees[i][z]-'0'){
                    break;
                }
                
            }
            //
            //std::cout<<thi<<" "<<top<<std::endl;
            //std::cout<<thi<<" | t = "<<top<<" ; b = "<<bottom<<" ; l = "<<left<<" ; r = "<<right<<" v? "<<(thi<top&&thi<bottom&&thi<left&&thi<right)<<std::endl;
            if(thi<=top&&thi<=bottom&&thi<=left&&thi<=right)
                total++;
            if(top_dis*bottom_dis*left_dis*right_dis>p2_score)
                p2_score=top_dis*bottom_dis*left_dis*right_dis;

        }
    }
    }
  }
  std::cout<<trees.size()<<" "<<trees[0].size()<<" ";
  int per = 2*(trees.size()+trees[0].size());
  int all_inner = (trees.size())*(trees[0].size())-per;
  std::cout<<"part 1 :"<<(all_inner-total)+per<<std::endl;
  std::cout<<"part 2 :"<<p2_score;
  return 0;
}