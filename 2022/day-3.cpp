#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
int getcc(char c){
    if(((int)c)-96>0)//lowercase
        return((int)c)-96;
    else
        return((int)c-38);
}
int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  std::vector<std::string> inp;
  std::string line;
  while(getline(std::cin, line)){
    if(line==";")
      break;
    inp.push_back(line);
  }
  int total = 0;
  for(std::string ss : inp){
    std::vector<char> contains = {};
    std::string s1 = ss.substr(0,ss.size()/2);
    std::string s2 = ss.substr(ss.size()/2,ss.size());
    for(int i = 0; i!=s1.size();i++){
        
        for(int z = 0; z!=s2.size();z++){
            if(s1[i]==s2[z]&&!std::count(contains.begin(), contains.end(), s1[i]))
                contains.push_back(s1[i]);
        }
    }
    for(char cc : contains){
        total+=getcc(cc);
    }
  }
  std::cout<<"part 1 :"<<total<<std::endl;
  total = 0;
  for(int i = 0; i!=inp.size();i+=3){
    std::string ps1 = inp[i];
    std::string ps2 = inp[i+1];
    std::string ps3 = inp[i+2];
    std::vector<char> contains = {};
    for(int z = 0;z!=ps1.size();z++){
        for(int y = 0;y!=ps2.size();y++){
            if(z!=y&&ps1[z]==ps2[y]){
                for(int w = 0;w!=ps3.size();w++){
                    if(ps1[z]==ps3[w]&&!std::count(contains.begin(), contains.end(), ps1[z]))
                contains.push_back(ps1[z]);
                }
            }
        }
    }
    for(char cc : contains){
        total+=getcc(cc);
    }
  }
  std::cout<<"part 2 :"<<total;
  return 0;
}