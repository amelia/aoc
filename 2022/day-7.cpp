#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  std::vector<int> inp;
  std::string line;
  std::string wd = "/";
  std::map<std::string,int> tree;
  while(getline(std::cin, line)){
    if(line==";")
      break;
    if(line[0]=='$'){
        if(line[2]=='c'){
            //change dir
            line.erase(0,5);
            if(line==".."){
                wd.erase(wd.size()-1,1);
                for(int i = wd.size();i!=0;i--){
                    if(wd[i]=='/')
                        break;
                    wd.erase(i,1);
                }
            } else {
            wd+=line+"/";
            if(line=="/")
                wd="/";
            }
        } else {
            //list dir, just ignore lmao
            tree.insert(std::pair<std::string,int>(wd,0));
        }

    } else {
        if(line[0]!='d'){
            //not a dir
            std::string temp = "";
            for(char c : line){
                if(c==' ')
                    break;
                temp+=c;
            }
            tree[wd]+=stoi(temp);
            std::map<std::string, int>::iterator itr1;
            for (itr1 = tree.begin(); itr1 != tree.end(); ++itr1) {
                if(wd.find(itr1->first) != std::string::npos&&wd!="/"&&itr1->first!=wd){
                    tree[itr1->first]+=stoi(temp);
                }
            }
        }
    }
    
    //std::cout<<std::endl<<wd<<std::endl;
    
  }
  int MAX = 70000000;
  int NEEDED = 30000000;
  int unused = MAX-tree["/"];
  int total = 0;
  int p2_total = 0;
  std::map<std::string, int>::iterator itr;
    for (itr = tree.begin(); itr != tree.end(); ++itr) {
        //std::cout << '\t' << itr->first << '\t' << itr->second<< '\n';
        if(itr->second<=100000){
            total+=itr->second;
        }

    }
    std::vector<int> possible = {};
    for (itr = tree.begin(); itr != tree.end(); ++itr) {
        if(unused+itr->second>=NEEDED){
            possible.push_back(itr->second);
        }
    }
    std::sort(possible.begin(),possible.end());
    std::cout<<"part 1 :"<<total<<std::endl;
    std::cout<<"part 2 :"<<possible[0];
  return 0;
}