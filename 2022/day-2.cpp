#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
int rps(char user, char opp){ // 0 = lose, 1 = win, 2 = draw
    if(user=='X'){
        if(opp=='A')
            return 2;
        if(opp=='B')
            return 0; 
        if(opp=='C')
            return 1; 
    } else if(user=='Y'){
        if(opp=='A')
            return 1;
        if(opp=='B')
            return 2; 
        if(opp=='C')
            return 0; 
    } 
    if(opp=='A')
        return 0;
    if(opp=='B')
        return 1; 
    if(opp=='C')
        return 2; 
    return 3;
}
int p2_rps(char user,char opp){
    int p = 0;
    if(user=='Y'){
        if(opp=='A'){
            return 1+3;
        } else if(opp=='B'){
            return 2+3;
        }
        return 3+3;
    }
    else if(user=='Z'){
        if(opp=='A'){
            return 2+6;
        } else if(opp=='B'){
            return 3+6;
        }
        return 1+6;
    }
    if(opp=='A'){
        return 3;
    } else if(opp=='B'){
        return 1;
    }
    return 2;
    

}
int main(int argc, char* argv[]){
  std::cout<<"paste input followed by a ;\n";
  std::vector<int> inp;
  std::string line;
  int points = 0;
  int p2_points = 0;
  while(getline(std::cin, line)){
    if(line==";")
      break;
    if(rps(line[2],line[0])==2)
        points+=3;
    else if(rps(line[2],line[0])==1)
        points+=6;

    if(line[2]=='X')
        points+=1;
    else if(line[2]=='Y')
        points+=2;
    else 
        points+=3;
    //split
    p2_points+=p2_rps(line[2],line[0]);
      
  }
  std::cout<<"part 1 :"<<points<<std::endl;
  std::cout<<"part 2 :"<<p2_points;
  //std::cout<<"part 1 :"<<inp[0]<<std::endl; 
  //std::cout<<"part 2 :"<<inp[0]+inp[1]+inp[2];
  return 0;
}