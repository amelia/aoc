set windows-shell := ["cmd.exe", "/c"]
#list all options
default:
    just --list
#list total lines of c++ code
lines:
    wc -l */*.cpp
#2022 day 1 ~ both parts
y22-d1:
    g++ -o ./2022/day-1 ./2022/day-1.cpp && ./2022/day-1 
#2022 day 2 ~ both parts
y22-d2:
    g++ -o ./2022/day-2 ./2022/day-2.cpp && ./2022/day-2
#2022 day 3 ~ both parts
y22-d3:
    g++ -o ./2022/day-3 ./2022/day-3.cpp && ./2022/day-3
#2022 day 4 ~ both parts
y22-d4:
    g++ -o ./2022/day-4 ./2022/day-4.cpp && ./2022/day-4
#2022 day 5 ~ both parts
y22-d5:
    g++ -o ./2022/day-5 ./2022/day-5.cpp && ./2022/day-5  
#2022 day 6 ~ both parts
y22-d6:
    g++ -o ./2022/day-6 ./2022/day-6.cpp && ./2022/day-6
#2022 day 7 ~ both parts
y22-d7:
    g++ -o ./2022/day-7 ./2022/day-7.cpp && ./2022/day-7 
#2022 day 8 ~ both parts
y22-d8:
    g++ -o ./2022/day-8 ./2022/day-8.cpp && ./2022/day-8  
#2022 day 9 ~ (not done lol) both parts
y22-d9:
    g++ -o ./2022/day-9 ./2022/day-9.cpp && ./2022/day-9 
#2022 day 10 ~ both parts
y22-d10:
    g++ -o ./2022/day-10 ./2022/day-10.cpp && ./2022/day-10 